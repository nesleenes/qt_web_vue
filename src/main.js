import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import store from './store';
import { VuejsDatatableFactory } from 'vuejs-datatable';
 
Vue.use( VuejsDatatableFactory );
import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);

import DefaultLayout from '@/layout/DefaultLayout.vue';
import AuthLayout from '@/layout/AuthLayout.vue';
// import CleanLayout from "@/layout/CleanLayout.vue";
import QuotationList from '@/quotations/List.vue';
import QuotationAdd from '@/quotations/Add.vue';
import BillAdd from '@/quotations/Bill.vue';
import ReceiptAdd from '@/quotations/Receipt.vue';
import QuotationEdit from '@/quotations/Edit.vue';
import QuotationDetail from '@/quotations/Detail.vue';
import UserEdit from '@/me/Edit.vue';
import UserProfile from '@/me/Profile.vue';
import Dashboard from '@/dashboard/Dashboard.vue';
// import LoginSession from '@/authSession'

import UserEdits from '@/setting/user/Edit.vue';
import User from '@/setting/user/Index.vue';
import Login from '@/auth/Login.vue';
import Forgotpassword from '@/auth/ForgotPassword.vue';
import UserAdd from '@/setting/user/Add.vue';
import DetailUser from '@/setting/user/Detail.vue';

// const session = LoginSession.getSession();
// if (!!session) {
//   axios.defaults.headers.common['Authorization'] = 'Bearer ' + session.access_token;
//   Vue.prototype.$accessToken = session.access_token;
// } else {
//   Vue.prototype.$accessToken = "";
// }

Vue.use(VueAxios, axios);

Vue.use(VueRouter);
Vue.config.productionTip = false;

const routes = [
  {
    path: '/',
    component: DefaultLayout,
    children: [
      {
        path: 'profiles',
        component: UserProfile,
      },
      {
        path: 'profiles/edits',
        component: UserEdit,
      },
    ],
    meta: { requiresAuth: true },
  },

  {
    path: '/',
    component: AuthLayout,
    children: [
      {
        path: '/auth/login',
        component: Login,
      },
      {
        path: '/auth/forgotpassword',
        component: Forgotpassword,
      },
    ],
  },
  {
    path: '/dashboard',
    component: DefaultLayout,
    children: [
      {
        path: '/',
        component: Dashboard,
      },
    ],
    meta: { requiresAuth: true },
  },
  {
    path: '/quotations',
    component: DefaultLayout,
    children: [
      {
        path: '/',
        component: QuotationList,
      },
      {
        path: 'adds',
        component: QuotationAdd,
      },
      {
        path: 'adds/bills',
        component: BillAdd,
      },
      {
        path: 'adds/receipts',
        component: ReceiptAdd,
      },
      {
        path: 'edits/:id',
        component: QuotationEdit,
      },
      {
        path: 'details/:id',
        component: QuotationDetail,
      },
    ],
    meta: { requiresAuth: true },
  },
  {
    path: '/settings',
    component: DefaultLayout,
    children: [
      {
        path: 'users',
        component: User,
      },
      {
        path: 'user/adds',
        component: UserAdd,
      },
      {
        path: 'user/:id/edits',
        component: UserEdits,
      },
      {
        path: 'user/detail',
        component: DetailUser,
      },
    ],
    meta: { isAdmin: true, requiresAuth: true },
  },
];

const router = new VueRouter({
  mode: 'history',
  routes: routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!localStorage.getItem('access_token')) {
      next('/auth/login');
    } else {
      next();
    }
  } else {
    next();
  }

  if (to.matched.some((record) => record.meta.isAdmin)) {
    if (localStorage.getItem('role') !== 'admin') {
      next('/dashboard');
    } else {
      next();
    }
  } else {
    next();
  }
});

new Vue({
  App,
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

export default router;
